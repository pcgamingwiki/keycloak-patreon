package com.pcgamingwiki.keycloak.patreon;

import com.fasterxml.jackson.databind.JsonNode;
import org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.broker.social.SocialIdentityProvider;
import org.keycloak.events.EventBuilder;
import org.keycloak.models.KeycloakSession;

public class PatreonIdentityProvider
        extends AbstractOAuth2IdentityProvider<PatreonIdentityProviderConfig>
        implements SocialIdentityProvider<PatreonIdentityProviderConfig> {

    public static final String AUTH_URL = "https://www.patreon.com/oauth2/authorize";
    public static final String TOKEN_URL = "https://www.patreon.com/api/oauth2/token";
    public static final String PROFILE_URL = "https://www.patreon.com/api/oauth2/v2/identity";
    public static final String DEFAULT_SCOPE = "identity identity[email]";

    public PatreonIdentityProvider(KeycloakSession session, PatreonIdentityProviderConfig config) {
        super(session, config);
        config.setAuthorizationUrl(AUTH_URL);
        config.setTokenUrl(TOKEN_URL);
        config.setUserInfoUrl(PROFILE_URL);
    }

    @Override
    protected BrokeredIdentityContext extractIdentityFromProfile(EventBuilder event, JsonNode data) {
        JsonNode profile = data.get("data");
        JsonNode attribtues = profile.get("attributes");

        BrokeredIdentityContext user = new BrokeredIdentityContext(getJsonProperty(profile, "id"));

        user.setFirstName(getJsonProperty(attribtues, "first_name"));
        user.setLastName(getJsonProperty(attribtues, "last_name"));
        user.setEmail(getJsonProperty(attribtues, "email"));
        user.setIdpConfig(getConfig());
        user.setIdp(this);

        return user;
    }

    protected String getDefaultScopes() {
        return DEFAULT_SCOPE;
    }
}
