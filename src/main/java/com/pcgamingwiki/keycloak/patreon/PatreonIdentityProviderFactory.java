package com.pcgamingwiki.keycloak.patreon;

import org.keycloak.broker.provider.AbstractIdentityProviderFactory;
import org.keycloak.broker.social.SocialIdentityProviderFactory;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;

public class PatreonIdentityProviderFactory
        extends AbstractIdentityProviderFactory<PatreonIdentityProvider>
        implements SocialIdentityProviderFactory<PatreonIdentityProvider> {

    public static final String PROVIDER_ID = "patreon";
    public static final String PROVIDER_NAME = "Patreon";

    public String getName() {
        return PROVIDER_NAME;
    }

    public PatreonIdentityProvider create(KeycloakSession keycloakSession, IdentityProviderModel identityProviderModel) {
        return new PatreonIdentityProvider(keycloakSession, new PatreonIdentityProviderConfig(identityProviderModel));
    }

    @SuppressWarnings("unchecked")
    @Override
    public PatreonIdentityProviderConfig createConfig() {
        return new PatreonIdentityProviderConfig();
    }


    public String getId() {
        return PROVIDER_ID;
    }
}
